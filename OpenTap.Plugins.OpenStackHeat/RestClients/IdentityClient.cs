﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Objects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Version = OpenTap.Plugins.OpenStackHeat.Objects.Version;
using Settings = OpenTap.Plugins.OpenStackHeat.OpenstackPluginSettings;

namespace OpenTap.Plugins.OpenStackHeat.RestClients
{
    /// <summary>
    /// Provides a minimal client for Identity service in OpenStack.
    /// </summary>
    public /* fix to internal*/ static class IdentityClient
    {
        private static readonly string knownVersion = "v3";

        /// <summary>
        /// Gets the REST API versions
        /// Relationship: https://docs.openstack.org/api/openstack-identity/3/rel/auth_tokens
        /// </summary>
        /// <param name="identityServiceEndpoint">The Indentity Service Endpoint.</param>
        /// <returns>The authentication token</returns>
        public static async Task<RestClientResult<List<Version>>> GetVersions(string identityServiceEndpoint)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=list-versions-detail#list-versions

            // Response Parameters
            // +------------------------+--------+--------+--------------------------------------------------------------+
            // | Name                   | In     | Type   | Description                                                  |
            // +------------------------+--------+--------+--------------------------------------------------------------+
            // | X-Openstack-Request-Id	| header | string | A unique ID for tracking service request. The request ID     |
            // |                        |        |        | associated with the request by default appears in the service|
            // |                        |        |        | logs.                                                        |
            // | versions               | body   | array  | A list of all orchestration API versions. Each object in the |
            // |                        |        |        | list provides information about a supported API version such |
            // |                        |        |        | as id, status and links, among other fields.                 |
            // | id                     | body   | string | A common name for the version in question.Informative only,  |
            // |                        |        |        | it has no real semantic meaning.                             |
            // | status                 | body   | string | The status of this API version. This can be one of:          |
            // |                        |        |        | - CURRENT: this is the preferred version of the API to use   |
            // |                        |        |        | - SUPPORTED: this is an older, but still supported version of|
            // |                        |        |        |       the API                                                |
            // |                        |        |        | - DEPRECATED: a deprecated version of the API that is slated |
            // |                        |        |        |       for removal                                            |
            // | links                  | body   | array  | A list of URLs for the stack. Each URL is a JSON object with |
            // |                        |        |        | an href key indicating the URL and a rel key indicating its  |
            // |                        |        |        | relationship to the stack in question. There may be multiple |
            // |                        |        |        | links returned. The self relationship identifies the URL of  |
            // |                        |        |        | the stack itself.                                            |
            // +------------------------+--------+--------+--------------------------------------------------------------+

            string message = null;
            List<Version> versions = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync(identityServiceEndpoint);
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.MultipleChoices:
                            // 300 - Multiple Choices There are multiple choices for resources.The request has to be more specific to successfully retrieve one of these resources.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                GetVersionsValueResponse gvr = serializer.Deserialize<GetVersionsValueResponse>(jsonReader);
                                if(gvr != null && gvr.Versions != null && gvr.Versions.Versions != null)
                                {
                                    versions = gvr.Versions.Versions;
                                }
                                else
                                {
                                    message = responseMessage.Content.ReadAsStringAsync().Result;
                                }
                            }
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<List<Version>>(message, versions);
        }

        /// <summary>
        /// Authenticates an identity and generates a token. Uses the password authentication method and scopes
        /// authorization to a project, domain, or the system. The request body must include a payload that specifies
        /// the password authentication method which includes the credentials in addition to a project, domain, or
        /// system authorization scope.
        /// Relationship: https://docs.openstack.org/api/openstack-identity/3/rel/auth_tokens
        /// </summary>
        /// <param name="identityServiceEndpoint">The Indentity Service Endpoint.</param>
        /// <param name="domainName">The domain name in which the project can be found.</param>
        /// <param name="projectId">The project Id to identify to which project the user should be authenticate against.</param>
        /// <param name="theUser">The username to authenticate.</param>
        /// <param name="thePassword">The password for the user.</param>
        /// <returns>The authentication token</returns>
        public static async Task<RestClientResult<string>> Authenticate(string identityServiceEndpoint, string domainName, string projectId, string theUser, string thePassword)
        {
            // https://developer.openstack.org/api-ref/identity/v3/index.html?expanded=password-authentication-with-scoped-authorization-detail#password-authentication-with-scoped-authorization

            string token = null;
            string message = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                JObject o = JObject.FromObject(new
                {
                    auth = new
                    {
                        identity = new
                        {
                            methods = new JArray
                            {
                                "password"
                            },
                            password = new
                            {
                                user = new
                                {
                                    name = theUser,
                                    domain = new
                                    {
                                        //id = "default",
                                        //name = "Default"
                                        //id = "63e4e021a7ea4ebe88f2ea99429334ec",
                                        name = domainName
                                    },
                                    password = thePassword
                                }
                            }
                        },
                        scope = new
                        {
                            project = new
                            {
                                id = projectId
                            }
                        }
                    }
                });

                //StringContent stringContent = new StringContent(o.ToString());
                try
                {
                    using (ByteArrayContent byteArrayContent = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(o.ToString())))
                    {
                        HttpResponseMessage responseMessage = await httpClient.PostAsync($"{identityServiceEndpoint}/{knownVersion}/auth/tokens", byteArrayContent);
                        switch (responseMessage.StatusCode)
                        {
                            case HttpStatusCode.Created:
                                message = "Resource was created and is ready to use.";
                                token = responseMessage.Headers.GetValues("X-Subject-Token").FirstOrDefault();
                                break;
                            case HttpStatusCode.BadRequest:
                                message = "Bad Request -> Some content in the request was invalid.";
                                break;
                            case HttpStatusCode.Unauthorized:
                                message = "Unauthorized -> User must authenticate before making a request.";
                                break;
                            case HttpStatusCode.Forbidden:
                                message = "Forbidden-> Policy does not allow current user to do this operation.";
                                break;
                            case HttpStatusCode.NotFound:
                                message = "Not Found -> The requested resource could not be found.";
                                break;
                            default:
                                message = "Unexpected API behavior";
                                break;
                        }

                        using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                        using (StreamReader streamReader = new StreamReader(stream))
                        using (JsonReader jsonReader = new JsonTextReader(streamReader))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            AuthenticationResponse ar = serializer.Deserialize<AuthenticationResponse>(jsonReader);
                        }
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }
            return new RestClientResult<string>(message, token);
        }

        /// <summary>
        /// Revokes a token.
        /// This call is similar to the HEAD /auth/tokens call except that the X-Subject-Token token is immediately not
        /// valid, regardless of the expires_at attribute value.An additional X-Auth-Token is not required.
        /// Relationship: https://docs.openstack.org/api/openstack-identity/3/rel/auth_tokens
        /// </summary>
        /// <param name="identityServiceEndpoint">The Indentity Service Endpoint.</param>
        /// <param name="token">The token to be revoked</param>
        /// <returns>A boolean indicating whether the token has been revoked or not.</returns>
        public static async Task<RestClientResult<bool>> RevokeToken(string identityServiceEndpoint, string token)
        {
            // https://developer.openstack.org/api-ref/identity/v3/index.html?expanded=password-authentication-with-scoped-authorization-detail,check-token-detail,revoke-token-detail#revoke-token

            // X-Auth-Token    header  string    A valid authentication token for an administrative user.
            // X-Subject-Token header  string    The authentication token.An authentication response returns the token ID in this header rather than in the response body.

            string message = null;
            bool revoked = false;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);
                httpClient.DefaultRequestHeaders.Add("X-Subject-Token", token);

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.DeleteAsync($"{identityServiceEndpoint}/{knownVersion}/auth/tokens");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.Created:
                            message = "Resource was created and is ready to use.";
                            revoked = true; // -> seems like Identity returns NoContent in case of success
                            break;
                        case HttpStatusCode.BadRequest:
                            message = "Bad Request -> Some content in the request was invalid.";
                            break;
                        case HttpStatusCode.Unauthorized:
                            message = "Unauthorized -> User must authenticate before making a request.";
                            break;
                        case HttpStatusCode.Forbidden:
                            message = "Forbidden -> Policy does not allow current user to do this operation.";
                            break;
                        case HttpStatusCode.NotFound:
                            message = "Not Found -> The requested resource could not be found.";
                            break;
                        case HttpStatusCode.NoContent:
                            message = "It is not documented that in case of successed operation NoContent will be returned, however this seems to be the case.";
                            revoked = true;
                            break;
                        default:
                            message = "Unexpected API behavior.";
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms."; 
                }

                // no response
                // return the boolean indicating the whether the token has been revoked or not
                return new RestClientResult<bool>(message, revoked);
            }
        }
    }
}
