﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackHeat.RestClients
{
    /// <summary>
    /// Rest Client Result. Helper class that all the rest client calls will make use of.
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class RestClientResult<TResult>
    {
        #region Properties

        /// <summary>
        /// A text message accompanying the Result. Typically it will be the content sent by the server, f.ex.:
        /// {
        ///   "code": 400,
        ///   "title": "Bad Request",
        ///   "explanation": "The server could not comply with the request since it is either malformed or otherwise incorrect.",
        ///   "error":
        ///   {
        ///     "type": "StackValidationFailed",
        ///     "traceback": null,
        ///     "message": "Property error: : resources.dummy_server.properties.image: : Error validating value 'cirros-0.3.5': No images matching {'name': 'cirros-0.3.5'}."
        ///   }
        /// }
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Gets the result value of this <see cref="ClientResponse">ClientResponse</see>.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public TResult Result { get; }

        #endregion Properties

        #region ctor

        /// <summary>
        /// Creates a new instance of <see cref="ClientResponse">ClientResponse</see>
        /// </summary>
        /// <param name="message">A text message accompanying the result.</param>
        /// <param name="result">The actual result.</param>
        public RestClientResult(string message, TResult result)
        {
            Message = message;
            Result = result;
        }

        #endregion ctor
    }

    /// <summary>
    /// Helper class to simplify the execution and retrieval of data on the REST-client side.
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class ClientTask<TResult> : Task<TResult>
    {
        #region Properties

        /// <summary>
        /// The response message.
        /// </summary>
        public string Message { get; set; }

        #endregion Properties

        #region ctor

        /// <summary>
        /// Initializes a new instance of <see cref="ClientTask">ClientTask</see> with the specified function
        /// </summary>
        /// 
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        ///   
        /// <param name="function">The delegate that represents the code to execute in the task. When the function
        /// has completed, the task's System.Threading.Tasks.Task`1.Result property will be set to return the result
        /// value of the function.</param>
        public ClientTask(Func<TResult> function)
            : base(function)
        {
        }

        /// <summary>
        /// Initializes a new instance of <see cref="ClientTask">ClientTask</see> with the specified function.
        /// </summary>
        /// 
        /// <exception cref="System.ObjectDisposedException">The System.Threading.CancellationTokenSource that created
        /// cancellationToken has already been disposed.</exception>
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        /// 
        /// <param name="function"></param>
        /// <param name="cancellationToken"></param>
        public ClientTask(Func<TResult> function, CancellationToken cancellationToken)
            : base(function, cancellationToken)
        {
        }

        /// <summary>
        /// Initializes a new instance of <see cref="ClientTask">ClientTask</see> with the specified function and
        /// creation options.
        /// </summary>
        /// 
        /// <exception cref="System.ArgumentOutOfRangeException">The creationOptions argument specifies an invalid
        /// value for System.Threading.Tasks.TaskCreationOptions.</exception>
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        /// 
        /// <param name="function">The delegate that represents the code to execute in the task. When the function
        /// has completed, the task's System.Threading.Tasks.Task`1.Result property will be set to return the result
        /// value of the function.</param>
        /// <param name="creationOptions">The System.Threading.CancellationToken to be assigned to this task.</param>
        public ClientTask(Func<TResult> function, TaskCreationOptions creationOptions)
            : base(function, creationOptions)
        {
        }

        /// <summary>
        /// Initializes a new instance of <see cref="ClientTask">ClientTask</see> with the specified function and
        /// state.
        /// </summary>
        /// 
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        /// 
        /// <param name="function">The delegate that represents the code to execute in the task. When the function
        /// has completed, the task's System.Threading.Tasks.Task`1.Result property will be set to return the result
        /// value of the function.</param>
        /// <param name="state">An object representing data to be used by the action.</param>
        public ClientTask(Func<object, TResult> function, object state)
            : base(function, state)
        {
        }

        /// <summary>
        /// Initializes a new instance of <see cref="ClientTask">ClientTask</see> with the specified function and
        /// creation options.
        /// </summary>
        /// 
        /// <exception cref="System.ObjectDisposedException">The System.Threading.CancellationTokenSource that created
        /// cancellationToken has already been disposed.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">The creationOptions argument specifies an invalid
        /// value for System.Threading.Tasks.TaskCreationOptions.</exception>
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        /// 
        /// <param name="function">The delegate that represents the code to execute in the task. When the function has
        /// completed, the task's System.Threading.Tasks.Task`1.Result property will be set to return the result value
        /// of the function.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken that will be assigned to the new
        /// task.</param>
        /// <param name="creationOptions">The System.Threading.Tasks.TaskCreationOptions used to customize the task's
        /// behavior.</param>
        public ClientTask(Func<TResult> function, CancellationToken cancellationToken, TaskCreationOptions creationOptions)
            : base(function, cancellationToken, creationOptions)
        {
        }

        /// <summary>
        /// Initializes a new instance of <see cref="ClientTask">ClientTask</see> with the specified action, state,
        /// and options.
        /// </summary>
        /// 
        /// <exception cref="System.ObjectDisposedException">The System.Threading.CancellationTokenSource that created
        /// cancellationToken has already been disposed.</exception>
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        /// 
        /// <param name="function">The delegate that represents the code to execute in the task. When the function
        /// has completed, the task's System.Threading.Tasks.Task`1.Result property will be set to return the result
        /// value of the function.</param>
        /// <param name="state">An object representing data to be used by the function.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken to be assigned to the new task
        /// </param>
        public ClientTask(Func<object, TResult> function, object state, CancellationToken cancellationToken)
            : base(function, state, cancellationToken)
        {
        }
        
        /// <summary>
        /// Initializes a new instance of <see cref="ClientTask">ClientTask</see> with the specified action, state,
        /// and options.
        /// </summary>
        /// 
        /// <exception cref="System.ArgumentOutOfRangeException">The creationOptions argument specifies an invalid
        /// value for System.Threading.Tasks.TaskCreationOptions.</exception>
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        /// 
        /// <param name="function">The delegate that represents the code to execute in the task. When the function has
        /// completed, the task's System.Threading.Tasks.Task`1.Result property will be set to return the result value
        /// of the function.</param>
        /// <param name="state">An object representing data to be used by the function.</param>
        /// <param name="creationOptions">The System.Threading.Tasks.TaskCreationOptions used to customize the task's
        /// behavior.</param>
        public ClientTask(Func<object, TResult> function, object state, TaskCreationOptions creationOptions)
            : base(function, state, creationOptions)
        {
        }

        /// Initializes a new System.Threading.Tasks.Task`1 with the specified action, state, and options.
        /// </summary>
        /// 
        /// <exception cref="System.ObjectDisposedException">The System.Threading.CancellationTokenSource that created
        /// cancellationToken has already been disposed.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">The creationOptions argument specifies an invalid
        /// value for System.Threading.Tasks.TaskCreationOptions.</exception>
        /// <exception cref="System.ArgumentNullException">The function argument is null.</exception>
        /// 
        /// <param name="function">The delegate that represents the code to execute in the task. When the function has
        /// completed, the task's System.Threading.Tasks.Task`1.Result property will be set to return the result value
        /// of the function.</param>
        /// <param name="state">An object representing data to be used by the function.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken to be assigned to the new task
        /// </param>
        /// <param name="creationOptions">The System.Threading.Tasks.TaskCreationOptions used to customize the task's
        /// behavior.</param>
        public ClientTask(Func<object, TResult> function, object state, CancellationToken cancellationToken, TaskCreationOptions creationOptions)
            : base(function, state, cancellationToken, creationOptions)
        {
        }

        #endregion ctor
    }
}
