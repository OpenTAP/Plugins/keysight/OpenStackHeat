﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Objects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Settings = OpenTap.Plugins.OpenStackHeat.OpenstackPluginSettings;

namespace OpenTap.Plugins.OpenStackHeat.RestClients
{
    public /* fix to internal*/ sealed class OrchestrationClient
    {
        private static readonly string knownVersion = "v1";

        #region General

        // +-------------------------+-------------+
        // | Functionality           | Status      |
        // +-------------------------+-------------+
        // | build_info              |             |
        // | get versions            | Implemented |
        // +-------------------------+-------------+

        /// <summary>
        /// Gets the REST API versions
        /// Relationship: https://docs.openstack.org/api/openstack-identity/3/rel/auth_tokens
        /// </summary>
        /// <param name="identityServiceEndpoint">The Indentity Service Endpoint.</param>
        /// <returns>The authentication token</returns>
        public static async Task<RestClientResult<List<Version>>> GetVersions(string identityServiceEndpoint)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=list-versions-detail#list-versions

            // Response Parameters
            // +------------------------+--------+--------+--------------------------------------------------------------+
            // | Name                   | In     | Type   | Description                                                  |
            // +------------------------+--------+--------+--------------------------------------------------------------+
            // | X-Openstack-Request-Id	| header | string | A unique ID for tracking service request. The request ID     |
            // |                        |        |        | associated with the request by default appears in the service|
            // |                        |        |        | logs.                                                        |
            // | versions               | body   | array  | A list of all orchestration API versions. Each object in the |
            // |                        |        |        | list provides information about a supported API version such |
            // |                        |        |        | as id, status and links, among other fields.                 |
            // | id                     | body   | string | A common name for the version in question.Informative only,  |
            // |                        |        |        | it has no real semantic meaning.                             |
            // | status                 | body   | string | The status of this API version. This can be one of:          |
            // |                        |        |        | - CURRENT: this is the preferred version of the API to use   |
            // |                        |        |        | - SUPPORTED: this is an older, but still supported version of|
            // |                        |        |        |       the API                                                |
            // |                        |        |        | - DEPRECATED: a deprecated version of the API that is slated |
            // |                        |        |        |       for removal                                            |
            // | links                  | body   | array  | A list of URLs for the stack. Each URL is a JSON object with |
            // |                        |        |        | an href key indicating the URL and a rel key indicating its  |
            // |                        |        |        | relationship to the stack in question. There may be multiple |
            // |                        |        |        | links returned. The self relationship identifies the URL of  |
            // |                        |        |        | the stack itself.                                            |
            // +------------------------+--------+--------+--------------------------------------------------------------+

            string message = null;
            List<Version> versions = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync(identityServiceEndpoint);
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.MultipleChoices:
                            // 300 - Multiple Choices There are multiple choices for resources.The request has to be more specific to successfully retrieve one of these resources.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                GetVersionsResponse gvr = serializer.Deserialize<GetVersionsResponse>(jsonReader);
                                if (gvr != null && gvr.Versions != null)
                                {
                                    versions = gvr.Versions;
                                }
                                else
                                {
                                    message = responseMessage.Content.ReadAsStringAsync().Result;
                                }
                            }
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<List<Version>>(message, versions);
        }

        #endregion

        #region Stacks

        // +-------------------------+-------------+
        // | Functionality           | Status      |
        // +-------------------------+-------------+
        // | create stack            | Implemented |
        // | preview stack           |             |
        // | list stacks             | Implemented |
        // | find stack              | Implemented |
        // | show stack details      | Implemented |
        // | update stack - put      |             |
        // | update stack - patch    |             |
        // | preview stack update    |             |
        // | preview stack patch     |             |
        // | find stack for deletion |             |
        // | delete stack            | Implemented |
        // | abandon stack           | Implemented |
        // | adopt stack             |             |
        // | export stack            |             |
        // | get stack template      |             |
        // | get stack environment   |             |
        // | get stack files         |             |
        // +-------------------------+-------------+

        /// <summary>
        /// Creates a stack.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="tenantId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackName">The name of the stack to be created.</param>
        /// <param name="hotFile">The Heat Orchestration Template File to be loaded. If null, no file will be loaded and a default template will be loaded.</param>
        /// <returns>The newly created stack details</returns>
        public static async Task<RestClientResult<Stack>> CreateStack(string orchestrationServiceEndpoint, string tenantId, string token, string stackName, string hotContentAsBase64)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=show-stack-details-detail#create-stack

            string message = null;
            Stack stack = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                string oo = "";

                if (hotContentAsBase64 != null)
                {
                    try
                    {
                        string hotFileContent = Base64Encoding.DecodeBase64(hotContentAsBase64);

                        JObject o = JObject.FromObject(new
                        {
                            disable_rollback = true,
                            files = new { },
                            parameters = new { },
                            stack_name = stackName,
                            template = hotFileContent,
                            tags = stackName,
                            timeout_mins = 60
                        });
                        oo = o.ToString();//.Replace("\\r", "\r").Replace("\\n", "\n");
                    }
                    catch (System.ArgumentNullException)
                    {
                        message = "path is null.";
                    }
                    catch (System.ArgumentException)
                    {
                        message = "path is a zero-length string, contains only white space, or contains one or more invalid characters as defined by System.IO.Path.InvalidPathChars.";
                    }
                    catch (System.IO.PathTooLongException)
                    {
                        message = "The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters.";
                    }
                    catch (System.IO.DirectoryNotFoundException)
                    {
                        message = "The specified path is invalid (for example, it is on an unmapped drive).";
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        message = "The file specified in path was not found.";
                    }
                    catch (System.IO.IOException)
                    {
                        message = "An I/O error occurred while opening the file.";
                    }
                    catch (System.UnauthorizedAccessException)
                    {
                        message = "path specified a file that is read-only.-or- This operation is not supported on the current platform.-or- path specified a directory.-or- The caller does not have the required permission.";
                    }
                    catch (System.NotSupportedException)
                    {
                        message = "path is in an invalid format.";
                    }
                    catch (System.Security.SecurityException)
                    {
                        message = "The caller does not have the required permission.";
                    }
                }
                else
                {
                    message = "The HOT file has not been specified!";
                }

                if (message == null) // if there is already an error, do not execute the following part
                {
                    using (ByteArrayContent byteArrayContent = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(oo)))
                    {
                        try
                        {
                            HttpResponseMessage responseMessage = await httpClient.PostAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{tenantId}/stacks", byteArrayContent);
                            switch (responseMessage.StatusCode)
                            {
                                case HttpStatusCode.Created:
                                    //message = "201 - Created -> Resource was created and is ready to use.";

                                    using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                                    using (StreamReader streamReader = new StreamReader(stream))
                                    using (JsonReader jsonReader = new JsonTextReader(streamReader))
                                    {
                                        JsonSerializer serializer = new JsonSerializer();
                                        StackContainer stackContainer = serializer.Deserialize<StackContainer>(jsonReader);
                                        stack = stackContainer?.Stack;
                                    }
                                    break;
                                case HttpStatusCode.BadRequest:
                                    //message = "400 - Bad Request -> Some content in the request was invalid.";
                                    message = responseMessage.Content.ReadAsStringAsync().Result;
                                    break;
                                case HttpStatusCode.Unauthorized:
                                    //message = "401 - Unauthorized -> User must authenticate before making a request.";
                                    message = responseMessage.Content.ReadAsStringAsync().Result;
                                    break;
                                case HttpStatusCode.Conflict:
                                    //message = "409 - Conflict -> This operation conflicted with another operation on this resource.";
                                    message = responseMessage.Content.ReadAsStringAsync().Result;
                                    //Log.Warning("Stack with the same name already existing");
                                    break;
                                default:
                                    //message = "Unexpected API behavior";
                                    message = responseMessage.Content.ReadAsStringAsync().Result;
                                    break;
                            }
                        }
                        catch (System.OperationCanceledException)
                        {
                            message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                        }
                    }
                }
            }

            return new RestClientResult<Stack>(message, stack);
        }

        /// <summary>
        /// Lists active stacks.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="tenantId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <returns>The stack list</returns>
        public static async Task<RestClientResult<string>> ListStacks(string orchestrationServiceEndpoint, string tenantId, string token)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=adopt-stack-detail,list-stacks-detail#list-stacks

            string message = null;
            string stacks = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{tenantId}/stacks");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //message = "Request was successful.";

                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            {
                                stacks = streamReader.ReadToEnd();
                            }
                            //using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            //{
                            //    JsonSerializer serializer = new JsonSerializer();
                            //    AuthenticationResponse ar = serializer.Deserialize<AuthenticationResponse>(jsonReader);
                            //}
                            break;
                        case HttpStatusCode.BadRequest:
                            //message = "Bad Request -> Some content in the request was invalid.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "Unauthorized-> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.InternalServerError:
                            //message = "Internal Server Error -> Something went wrong inside the service. This should not happen usually.If it does happen, it means the server has experienced some serious problems.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<string>(message, stacks);
        }

        /// <summary>
        /// Finds the details of the stack with the given stack Id.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="tenantId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackId">The Id of the Stack to be found.</param>
        /// <returns>The stack.</returns>
        public static async Task<RestClientResult<string>> FindStack(string orchestrationServiceEndpoint, string tenantId, string token, string stackId)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=adopt-stack-detail,list-stacks-detail#find-stack

            string message = null;
            string stacks = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{tenantId}/stacks/{stackId}");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //message = "Request was successful.";

                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            {
                                stacks = streamReader.ReadToEnd();
                            }
                            //using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            //{
                            //    JsonSerializer serializer = new JsonSerializer();
                            //    AuthenticationResponse ar = serializer.Deserialize<AuthenticationResponse>(jsonReader);
                            //}
                            break;
                        case HttpStatusCode.BadRequest:
                            //message = "Bad Request -> Some content in the request was invalid.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "Unauthorized-> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.InternalServerError:
                            //message = "Internal Server Error -> Something went wrong inside the service. This should not happen usually.If it does happen, it means the server has experienced some serious problems.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<string>(message, stacks);
        }

        /// <summary>
        /// Shows details for a stack.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="tenantId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackName">The name of the Stack.</param>
        /// <param name="stackId">The Id of the Stack.</param>
        /// <returns>The stack.</returns>
        public static async Task<RestClientResult<Stack>> ShowStackDetails(string orchestrationServiceEndpoint, string tenantId, string token, string stackName, string stackId)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=adopt-stack-detail,list-stacks-detail#show-stack-details

            string message = null;
            Stack stack = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{tenantId}/stacks/{stackId}");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //message = "Request was successful.";
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                StackContainer stackContainer = serializer.Deserialize<StackContainer>(jsonReader);
                                stack = stackContainer?.Stack;
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            //message = "Bad Request -> Some content in the request was invalid.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "Unauthorized -> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.InternalServerError:
                            //message = "Internal Server Error -> Something went wrong inside the service. This should not happen usually.If it does happen, it means the server has experienced some serious problems.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<Stack>(message, stack);
        }

        /// <summary>
        /// Deletes a stack. If a stack has snapshots, those snapshots are deleted as well.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="tenantId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackName">The name of the Stack.</param>
        /// <param name="stackId">The Id of the Stack.</param>
        /// <returns>The stack.</returns>
        public static async Task<RestClientResult<bool>> DeleteStack(string orchestrationServiceEndpoint, string tenantId, string token, string stackName, string stackId)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=show-stack-details-detail#delete-stack

            string message = null;
            bool deleted = false;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.DeleteAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{tenantId}/stacks/{stackId}");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.NoContent:
                            //message = "204 - No Content	The server has fulfilled the request by deleting the resource.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            deleted = true;
                            break;
                        case HttpStatusCode.BadRequest:
                            //message = "400 - Bad Request -> Some content in the request was invalid.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "401 - Unauthorized -> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.NotFound:
                            //message = "404 - Not Found -> The requested resource could not be found.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.InternalServerError:
                            //message = "500 - Internal Server Error -> Something went wrong inside the service. This should not happen usually.If it does happen, it means the server has experienced some serious problems.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<bool>(message, deleted);
        }

        /// <summary>
        /// Deletes a stack but leaves its resources intact, and returns data that describes the stack and its resources.
        /// This is a preview feature which has to be explicitly enabled by setting the following option in the heat.conf file:
        /// [DEFAULT]
        /// enable_stack_abandon = True
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="tenantId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackName">The name of a stack.</param>
        /// <param name="stackId">The UUID of the stack.</param>
        /// <returns>The stack.</returns>
        public static async Task<RestClientResult<bool>> AbandonStack(string orchestrationServiceEndpoint, string tenantId, string token, string stackName, string stackId)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=show-stack-details-detail#abandon-stack

            string message = null;
            bool deleted = false;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.DeleteAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{tenantId}/stacks/{stackId}");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //message = "200 - OK -> Request was successful.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            deleted = true;
                            break;
                        case HttpStatusCode.BadRequest:
                            //message = "400 - Bad Request -> Some content in the request was invalid.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "401 - Unauthorized -> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.NotFound:
                            //message = "404 - Not Found -> The requested resource could not be found.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.InternalServerError:
                            //message = "500 - Internal Server Error -> Something went wrong inside the service. This should not happen usually.If it does happen, it means the server has experienced some serious problems.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<bool>(message, deleted);
        }

        #endregion

        #region Stack Resources

        // +------------------------------+-------------+
        // | Functionality                | Status      |
        // +------------------------------+-------------+
        // | find stack resources         |             |
        // | list stack resources         |             |
        // | show resource data           |             |
        // | show resource metadata       |             |
        // | send signal to a resource    |             |
        // | mark a resource as unhealthy |             |
        // | list stack resources         |             |
        // +------------------------------+-------------+

        #endregion

        #region Stack Outputs

        // +---------------+-------------+
        // | Functionality | Status      |
        // +---------------+-------------+
        // | list outputs  | Implemented |
        // | show output   | Implemented |
        // +---------------+-------------+
        
        /// <summary>
        /// Lists outputs for a stack.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="projectId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackName">The name of a stack.</param>
        /// <param name="stackId">The UUID of the stack.</param>
        /// <returns></returns>
        public static async Task<RestClientResult<List<StackOutput>>> ListOutputs(string orchestrationServiceEndpoint, string projectId, string token, string stackName, string stackId)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=show-stack-details-detail,list-outputs-detail#list-outputs

            string message = null;
            List<StackOutput> outputs = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                // Request paramters
                // +------------+------+--------+--------------------------------------------------------------+
                // | Name       | In   | Type   | Description                                                  |
                // +------------+------+--------+--------------------------------------------------------------+
                // | tenant_id  | path | string | The UUID of the tenant. A tenant is also known as a project. |
                // | stack_name | path | string | The name of a stack.                                         |
                // | stack_id   | path | string | The UUID of the stack.                                       |
                // +------------+------+--------+--------------------------------------------------------------+                

                JObject o = JObject.FromObject(new
                {
                    tenant_id = projectId,
                    stack_name = stackName,
                    stack_id = stackId
                });

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{projectId}/stacks/{stackName}/{stackId}/outputs");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //message = "200 - OK -> Request was successful.";
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                ListOutputsResponse lor = serializer.Deserialize<ListOutputsResponse>(jsonReader);
                                outputs = lor?.Outputs;
                            }
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "401 - Unauthorized -> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.NotFound:
                            //message = "404 - Not Found -> The requested resource could not be found.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.InternalServerError:
                            //message = "500 - Internal Server Error -> Something went wrong inside the service. This should not happen usually.If it does happen, it means the server has experienced some serious problems.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<List<StackOutput>>(message, outputs);
        }

        /// <summary>
        /// Shows details for a stack output.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="projectId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackName">The name of a stack.</param>
        /// <param name="stackId">The UUID of the stack.</param>
        /// <param name="key">The key of a stack output</param>
        /// <returns></returns>
        public static async Task<RestClientResult<StackOutput>> ShowOutput(string orchestrationServiceEndpoint, string projectId, string token, string stackName, string stackId, string key)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=#show-output

            string message = null;
            StackOutput output = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                // Request paramters
                // +------------+------+--------+--------------------------------------------------------------+
                // | Name       | In   | Type   | Description                                                  |
                // +------------+------+--------+--------------------------------------------------------------+
                // | tenant_id  | path | string | The UUID of the tenant. A tenant is also known as a project. |
                // | stack_name | path | string | The name of a stack.                                         |
                // | stack_id   | path | string | The UUID of the stack.                                       |
                // | output_key | path | string | The key of a stack output.                                   |
                // +------------+------+--------+--------------------------------------------------------------+                

                JObject o = JObject.FromObject(new
                {
                    tenant_id = projectId,
                    stack_name = stackName,
                    stack_id = stackId
                });

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{projectId}/stacks/{stackName}/{stackId}/outputs/{key}");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //message = "200 - OK -> Request was successful.";
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                ShowOutputResponse sor = serializer.Deserialize<ShowOutputResponse>(jsonReader);
                                output = sor?.StackOutput;
                            }
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "401 - Unauthorized -> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.NotFound:
                            //message = "404 - Not Found -> The requested resource could not be found.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.InternalServerError:
                            //message = "500 - Internal Server Error -> Something went wrong inside the service. This should not happen usually.If it does happen, it means the server has experienced some serious problems.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<StackOutput>(message, output);
        }

        #endregion

        #region Stack Snapshots

        // +-------------------+-------------+
        // | Functionality     | Status      |
        // +-------------------+-------------+
        // | Snapshot a stack  |             |
        // | List snapshots    |             |
        // | Show snapshots    |             |
        // | Restore snapshot  |             |
        // | Delete a snapshot |             |
        // +-------------------+-------------+

        #endregion

        #region Stack Actions

        // +---------------------------------------------+-------------+
        // | Functionality                               | Status      |
        // +---------------------------------------------+-------------+
        // | Suspend stack                               |             |
        // | Resume stack                                |             |
        // | Cancel stack update                         |             |
        // | Cancel stack create/update without rollback |             |
        // | Check statck resources                      |             |
        // +---------------------------------------------+-------------+

        #endregion

        #region Events

        // +----------------------+-------------+
        // | Functionality        | Status      |
        // +----------------------+-------------+
        // | List stack events    | Implemented |
        // | Find stack events    |             |
        // | List resource events |             |
        // | Show event details   |             |
        // +----------------------+-------------+
        
        /// <summary>
        /// Lists events for a stack.
        /// </summary>
        /// <param name="orchestrationServiceEndpoint">The Orchestration Service Endpoint.</param>
        /// <param name="projectId">The id of the tenant(project).</param>
        /// <param name="token">Authentication token.</param>
        /// <param name="stackName">The name of a stack.</param>
        /// <param name="stackId">The UUID of the stack.</param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static async Task<RestClientResult<List<ResourceEvent>>> ListStackEvents(string orchestrationServiceEndpoint, string projectId, string token, string stackName, string stackId)
        {
            // https://developer.openstack.org/api-ref/orchestration/v1/index.html?expanded=#list-stack-events

            string message = null;
            List<ResourceEvent> events = null;

            using (var httpClient = new HttpClient() { Timeout = System.TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", token);

                // Request paramters
                // +--------+-----------------+-------+---------+-------------------------------------------------------------------------+
                // |Optional| Name            | In    | Type    | Description                                                             |
                // +--------+-----------------+-------+---------+-------------------------------------------------------------------------+
                // |        | tenant_id       | path  | string  | The UUID of the tenant. A tenant is also known as a project.            |
                // |        | stack_name      | path  | string  | The name of a stack.                                                    |
                // |        | stack_id        | path  | string  | The UUID of the stack.                                                  |
                // +--------+-----------------+-------+---------+-------------------------------------------------------------------------+
                // |  Yes   | resource_action | query | string  | Stack resource action. Valid resource actions are ADOPT, CHECK, CREATE, |
                // |        |                 |       |         | DELETE, INIT, RESTORE, RESUME, ROLLBACK, SNAPSHOT, SUSPEND, and UPDATE. |
                // |  Yes   | resource_status | query | string  | Stack resource status. Valid resource statuses are COMPLETE, FAILED and |
                // |        |                 |       |         | IN_PROGRESS.This can be specified more than once to filter the results  |
                // |        |                 |       |         | by multiple resource statuses.                                          |
                // |  Yes   | resource_name   | query | string  | Filters the result list by a resource name.You can use this filter      |
                // |        |                 |       |         | multiple times to filter by multiple resource names.                    |
                // |  Yes   | resource_type   | query | string  | Stack resource type. Valid resource types include OS::Cinder::Volume,   |
                // |        |                 |       |         | OS::Nova::Server, OS::Neutron::Port and so on. This parameter can be    |
                // |        |                 |       |         | specified more than once to filter results by multiple resource types.  |
                // |  Yes   | limit           | query | integer | Requests a page size of items. Returns a number of items up to a limit  |
                // |        |                 |       |         | value. Use the limit parameter to make an initial limited request and   |
                // |        |                 |       |         | use the ID of the last - seen item from the response as the marker      |
                // |        |                 |       |         | parameter value in a subsequent limited request.                        |
                // |  Yes   | marker          | query | string  | The ID of the last-seen item.Use the limit parameter to make an initial |
                // |        |                 |       |         | limited request and use the ID of the last - seen item from the response|
                // |        |                 |       |         | as the marker parameter value in a subsequent limited request.          |
                // |  Yes   | sort_keys       | query | string  | Sorts the result list by keys specified in this parameter.              |
                // |  Yes   | sort_dir        | query | string  | The sort direction of the list.A valid value is asc (ascending)or       |
                // |        |                 |       |         | desc(descending).                                                       |
                // |  Yes   | nested_depth    | query | integer | Includes resources from nested stacks up to the nested_depth levels of  |
                // |        |                 |       |         | recursion.                                                              |
                // +--------+-----------------+-------+---------+-------------------------------------------------------------------------+    

                JObject o = JObject.FromObject(new
                {
                    tenant_id = projectId,
                    stack_name = stackName,
                    stack_id = stackId
                });

                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{orchestrationServiceEndpoint}/{knownVersion}/{projectId}/stacks/{stackName}/{stackId}/events");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //message = "200 - OK -> Request was successful.";
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                ListStackEventsResponse lser = serializer.Deserialize<ListStackEventsResponse>(jsonReader);
                                events = lser?.Events;
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            //message = "400 - Bad Request -> Some content in the request was invalid.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Unauthorized:
                            //message = "401 - Unauthorized -> User must authenticate before making a request.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.NotFound:
                            //message = "404 - Not Found -> The requested resource could not be found.";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            //message = "Unexpected API behavior";
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }
            return new RestClientResult<List<ResourceEvent>>(message, events);
        }

        #endregion

        #region Templates

        // +-------------------------+-------------+
        // | Functionality           | Status      |
        // +-------------------------+-------------+
        // | List template versions  |             |
        // | List template functions |             |
        // | Validate template       |             |
        // +-------------------------+-------------+

        #endregion

        #region Software Configuration

        // +------------------------------------+-------------+
        // | Functionality                      | Status      |
        // +------------------------------------+-------------+
        // | Create configuration               |             |
        // | List configs                       |             |
        // | Show configuration details         |             |
        // | Delete config                      |             |
        // | Create deployment                  |             |
        // | List deployments                   |             |
        // | Show deployment details            |             |
        // | Update deployment                  |             |
        // | Delete deployment                  |             |
        // | Show server configuration metadata |             |
        // | List template functions            |             |
        // | Validate template                  |             |
        // +------------------------------------+-------------+

        #endregion

        #region Resource Types

        // +-----------------------------+-------------+
        // | Functionality               | Status      |
        // +-----------------------------+-------------+
        // | List resource types         |             |
        // | Show resource type template |             |
        // | Show resource type schema   |             |
        // +-----------------------------+-------------+

        #endregion

        #region Manage Service

        // +----------------------------------+-------------+
        // | Functionality                    | Status      |
        // +----------------------------------+-------------+
        // | Show orchestration engine status |             |
        // +----------------------------------+-------------+

        #endregion
    }
}
