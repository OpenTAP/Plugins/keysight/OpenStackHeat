﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Objects;
using OpenTap.Plugins.OpenStackHeat.RestClients;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackHeat
{
    [Display(Name: "Identity Service", Description: "The OpenStack' Identity service provides a single point of integration for managing authentication, authorization, and a catalog of services.", Groups: new string[] {"Heat"})]
    public sealed class IdentityServiceInstrument : Instrument
    {
        #region Settings 

        [Display(Name: "Domain Name", Group: "Settings")]
        public string DomainName { get; set; }

        [Display(Name: "Identity Service Endpoint", Group: "Settings")]
        public string Endpoint { get; set; }
        
        [Display(Name: "Password", Group: "Settings")]
        public string Password { get; set; }

        [Display(Name: "ProjectId", Group: "Settings")]
        public string ProjectId { get; set; }

        [Display(Name: "Authentication Token", Description: "The authentication token that is the result of the authentication operation.", Group: "Output")]
        [Output()]
        public string Token { get; set; }

        [Display(Name: "User", Group: "Settings")]
        public string User { get; set; }

        #endregion

        public IdentityServiceInstrument()
        {
            // OpenStack
            DomainName = "default";
            Endpoint = "http://identityService:5000";
            Name = "Identity Service";
            Password = "password";
            ProjectId = "projectId";
            Token = null;
            User = "user";

            // PackStack
            //Endpoint = "http://identityService:5000";
            //Name = "Identity Service";
            //Password = "password";
            //ProjectId = "projectId";
            //Token = null;
            //User = "user";

            Rules.Add(() => !string.IsNullOrEmpty(DomainName), "A domain name is required.", "DomainName");
            Rules.Add(() => !string.IsNullOrEmpty(Endpoint), "An endpoint is required.", "Endpoint");
            Rules.Add(() => !string.IsNullOrEmpty(Password), "A password is required.", "Password");
            Rules.Add(() => !string.IsNullOrEmpty(ProjectId), "A project Id is required.", "ProjectId");
            Rules.Add(() => !string.IsNullOrEmpty(User), "An user is required.", "User");
        }

        /// <summary>
        /// Closes the connection to the Identity Service
        /// </summary>
        public override void Close()
        {
            base.Close();

            Log.Debug("// Revoke Token ------------------------------------------------------------------------------------------");
            Task<RestClientResult<bool>> taskRevokeToken = IdentityClient.RevokeToken(Endpoint, Token);
            if (taskRevokeToken.Result.Result == false)
            {
                Log.Error($"Revoke token failed! Details: {taskRevokeToken.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Log.Info($"Token revoked successfully! Unauthenticated");

            }
            Log.Debug("----------------------------------------------------------------------------------------------------------");
        }

        /// <summary>
        /// Opens the connection to the Identity Service
        /// </summary>
        public override void Open()
        {
            base.Open();


            Log.Debug("// Version Handshake -------------------------------------------------------------------------------------");
            Task<RestClientResult<List<Version>>> taskGetVersions = IdentityClient.GetVersions(Endpoint);
            taskGetVersions.Wait();
            List<Version> versions = taskGetVersions.Result.Result;

            if (versions == null)
            {
                Log.Error($"Could not fetch API versions from EndPoint {Endpoint}!");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Version foundVersion = null;
                foreach (Version version in versions)
                {
                    if (version.Id == "v3.11" && version.Status == "stable")
                    {
                        foundVersion = version;
                        break;
                    }
                }
                if (foundVersion != null)
                {
                    Log.Info(foundVersion.ToString());
                }
                else
                {
                    Log.Error($"Unknown REST EndPoint {Endpoint}!");
                    foreach (Version version in versions)
                    {
                        Log.Error(versions.ToString());
                    }
                    throw new System.Exception("Execution cannot continue");
                }
            }
            Log.Debug("----------------------------------------------------------------------------------------------------------");


            Log.Debug("// Authenticate ------------------------------------------------------------------------------------------");
            Task<RestClientResult<string>> taskAuthenticate = IdentityClient.Authenticate(Endpoint, DomainName, ProjectId, User, Password);
            taskAuthenticate.Wait();
            Token = taskAuthenticate.Result.Result;
            if (Token == null)
            {
                Log.Error($"Authentication failed! Details: {taskAuthenticate.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Log.Info($"{User} authenticated on {Endpoint} {DomainName}/{ProjectId}");
            }
            Log.Debug("----------------------------------------------------------------------------------------------------------");
        }
    }
}
