﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenTap.Plugins.OpenStackHeat.Objects
{
    /// <summary>
    /// Get Versions Response class
    /// </summary>
    [JsonObject("GetVersionsResponse")]
    public class GetVersionsResponse
    {
        /// <summary>
        /// A list of all orchestration API versions. Each object in the list provides information about a supported
        /// API version such as id, status and links, among other fields.
        /// </summary>
        [JsonProperty("versions")]
        public List<Version> Versions { get; set; }
    }

    /// <summary>
    /// Version Values class
    /// </summary>
    [JsonObject("VersionValues")]
    public class VersionValues
    {
        /// <summary>
        /// A list of all orchestration API versions. Each object in the list provides information about a supported
        /// API version such as id, status and links, among other fields.
        /// </summary>
        [JsonProperty("values")]
        public List<Version> Versions { get; set; }
    }

    /// <summary>
    /// Get Versions Value Response
    /// </summary>
    [JsonObject("GetVersionsResponse")]
    public class GetVersionsValueResponse
    {
        /// <summary>
        /// Heat API version call contains an extra level called values
        /// 
        /// {
        ///   "versions": {
        ///     "values": [
        ///       {
        ///         "id": "v3.11",
        ///         "status": "stable",
        ///         "updated": "2018-10-15T00:00:00Z",
        ///         "links": [
        ///           {
        ///             "rel": "self",
        ///             "href": "http://192.168.11.134:5000/v3/"
        ///           }
        ///         ],
        ///         "media-types": [
        ///           {
        ///             "base": "application/json",
        ///             "type": "application/vnd.openstack.identity-v3+json"
        ///           }
        ///         ]
        ///       }
        ///     ]
        ///   }
        /// }
        /// instead of:
        /// {
        ///   'versions': [
        ///     {
        ///       'id': 'v1.0',
        ///       'status': 'CURRENT',
        ///       'links': [
        ///         {
        ///           'rel': 'self',
        ///           'href': 'http://10.114.182.105:8004/v1/'
        ///         }
        ///       ]
        ///     }
        ///   ]
        /// }
        ///
        /// </summary>
        [JsonProperty("versions")]
        public VersionValues Versions { get; set; }
    }

    /// <summary>
    /// Version class
    /// </summary>
    [JsonObject("Version")]
    public class Version
    {
        /// <summary>
        /// The status of this API version. This can be one of:
        ///  - CURRENT: this is the preferred version of the API to use
        ///  - SUPPORTED: this is an older, but still supported version of the API
        ///  - DEPRECATED: a deprecated version of the API that is slated for remova
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// A common name for the version in question. Informative only, it has no real semantic meaning.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }


        /// <summary>
        /// Time when the API was last updated. The date is formatted as f.ex.: '2018-10-15T00:00:00Z'
        /// </summary>
        [JsonProperty("updated")]
        public string Updated { get; set; }
        
        /// <summary>
        /// A list of URLs for the stack. Each URL is a JSON object with an href key indicating the URL and a rel key 
        /// indicating its relationship to the stack in question. There may be multiple links returned. The self
        /// relationship identifies the URL of the stack itself.
        /// </summary>
        [JsonProperty("links")]
        public List<Link> Links { get; set; }

        /// <summary>
        /// Reimplement ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            List<string> linksAsStrings = new List<string>();
            foreach(Link link in Links)
            {
                linksAsStrings.Add(link.ToString());
            }
            string linksAsString = string.Join(", ", linksAsStrings);

            return $"API version {Id} {Status} on {linksAsString}";
        }
    }

    /// <summary>
    /// Generic link class
    /// </summary>
    [JsonObject("Link")]
    public class Link
    {
        /// <summary>
        /// Url.
        /// </summary>
        [JsonProperty("href")]
        public string HRef { get; set; }

        /// <summary>
        /// Indicates the relationship to the stack in question
        /// </summary>
        [JsonProperty("rel")]
        public string Relation { get; set; }
        
        /// <summary>
        /// Reimplement ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Relation}:{HRef}";
        }
    }
}
