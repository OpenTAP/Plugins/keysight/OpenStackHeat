﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenTap.Plugins.OpenStackHeat.Objects
{
    /// <summary>
    /// Stack Container class
    /// </summary>
    [JsonObject()]
    public class StackContainer
    {
        [JsonProperty("stack")]
        public Stack Stack { get; set; }
    }

    /// <summary>
    ///  Stack Capability
    /// </summary>
    [JsonObject("Capability")]
    public class StackCapability
    {
    }

    /// <summary>
    /// Notification Topic
    /// </summary>
    [JsonObject("NotificationTopic")]
    public class StackNotificationTopic
    {
    }

    /// <summary>
    /// Show Output Response class
    /// </summary>
    [JsonObject("ShowOutputResponse")]
    public class ShowOutputResponse
    {
        /// <summary>
        /// Output.
        /// </summary>
        [JsonProperty("output")]
        public StackOutput StackOutput { get; set; }
    }

    /// <summary>
    /// List Outputs Response class
    /// </summary>
    [JsonObject("ListOutputsResponse")]
    public class ListOutputsResponse
    {
        /// <summary>
        /// Output.
        /// </summary>
        [JsonProperty("outputs")]
        public List<StackOutput> Outputs { get; set; }
    }

    /// <summary>
    /// Stack output class
    /// </summary>
    [JsonObject("output")]
    public class StackOutput
    {
        /// <summary>
        /// Output name.
        /// </summary>
        [JsonProperty("output_key")]
        public string Key { get; set; }

        /// <summary>
        /// Output value.
        /// </summary>
        [JsonProperty("output_value")]
        public string Value { get; set; }

        /// <summary>
        /// Output description.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Error message.
        /// </summary>
        [JsonProperty("output_error")]
        public string Error { get; set; }

        public override string ToString()
        {
            return $"{Key}:{Value} {Description} {Error}";
        }
    }

    /// <summary>
    /// Resource Events class
    /// </summary>
    [JsonObject("event")]
    public class ResourceEvent
    {
        /// <summary>
        /// The date and time when the event was created. The date and time stamp format is ISO 8601: 
        /// CCYY-MM-DDThh:mm:ss, for example, 2015-08-27T09:49:58.
        /// </summary>
        [JsonProperty("event_time")]
        public string EventTime { get; set; }

        /// <summary>
        /// The UUID of the event object.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// A list of URLs for the event. Each URL is a JSON object with an href key indicating the URL and a rel key 
        /// indicating its relationship to the event in question. There may be multiple links returned. The self 
        /// relationship identifies the URL of the event itself.
        /// </summary>
        [JsonProperty("links")]
        public List<Link> Links { get; set; }

        /// <summary>
        /// The ID of the logical stack resource.
        /// </summary>
        [JsonProperty("logical_resource_id")]
        public string LogicalResourceId { get; set; }

        /// <summary>
        /// The ID of the stack physical resource.
        /// </summary>
        [JsonProperty("physical_resource_id")]
        public string PhysicalResourceId { get; set; }

        /// <summary>
        /// The name of the resource.
        /// </summary>
        [JsonProperty("resource_name")]
        public string ResourceName { get; set; }

        /// <summary>
        /// The status of the resource.
        /// </summary>
        [JsonProperty("resource_status")]
        public string ResourceStatus { get; set; }

        /// <summary>
        /// The reason for the current stack resource state.
        /// </summary>
        [JsonProperty("resource_status_reason")]
        public string ResourceStatusReason { get; set; }

        public override string ToString()
        {
            return $"{EventTime}|{ResourceName}|{ResourceStatus}|{ResourceStatusReason}";
        }
    }

    /// <summary>
    /// Stack Event class
    /// </summary>
    [JsonObject("event")]
    public class StackEvent
    {
        /// <summary>
        /// The UUID of the event object.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// A list of URLs for the event. Each URL is a JSON object with an href key indicating the URL and a rel key 
        /// indicating its relationship to the event in question. There may be multiple links returned. The self 
        /// relationship identifies the URL of the event itself.
        /// </summary>
        [JsonProperty("links")]
        public List<Link> Links { get; set; }

        /// <summary>
        /// The name of the stack.
        /// </summary>
        [JsonProperty("stack_name")]
        public string StackName { get; set; }

        /// <summary>
        /// The description of the event.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// The status of the stack.
        /// </summary>
        [JsonProperty("stack_status")]
        public string StackStatus { get; set; }

        /// <summary>
        /// The reason for the current stack state.
        /// </summary>
        [JsonProperty("stack_status_reason")]
        public string StackStatusReason { get; set; }

        /// <summary>
        /// The date and time when the stack was created. The date and time stamp format is ISO 8601: 
        /// CCYY-MM-DDThh:mm:ss, for example, 2015-08-27T09:49:58.
        /// </summary>
        [JsonProperty("creation_time")]
        public string CreationTime { get; set; }

        /// <summary>
        /// The date and time when the stack was deleted. The date and time stamp format is ISO 8601: 
        /// CCYY-MM-DDThh:mm:ss, for example, 2015-08-27T09:49:58.
        /// </summary>
        [JsonProperty("deletion_time")]
        public string DeletionTime { get; set; }

        /// <summary>
        /// The date and time when the stack was updated. The date and time stamp format is ISO 8601: 
        /// CCYY-MM-DDThh:mm:ss, for example, 2015-08-27T09:49:58.
        /// </summary>
        [JsonProperty("updated_time")]
        public string UpdatedTime { get; set; }

        /// <summary>
        /// Stack Owner
        /// </summary>
        [JsonProperty("stack_owner")]
        public string StackOwner { get; set; }

        /// <summary>
        /// Parent
        /// </summary>
        [JsonProperty("parent")]
        public string Parent { get; set; }

        /// <summary>
        /// Stack User Project Id
        /// </summary>
        [JsonProperty("stack_user_project_id")]
        public string StackUserProjectId { get; set; }

        /// <summary>
        /// Tags
        /// </summary>
        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        public override string ToString()
        {
            return $"{StackName}|{StackStatus}|{StackStatusReason}";
        }
    }

    /// <summary>
    /// List of Stack Events Response class
    /// </summary>
    [JsonObject("ListStackEventsResponse")]
    public class ListStackEventsResponse
    {
        /// <summary>
        /// Array of events
        /// </summary>
        [JsonProperty("events")]
        public List<ResourceEvent> Events { get; set; }
    }

    /// <summary>
    /// Stack class
    /// </summary>
    [JsonObject("stack")]
    public class Stack
    {
        /// <summary>
        /// Capabilities of this stack.
        /// </summary>
        [JsonProperty("capabilities")]
        public List<StackCapability> Capabilities { get; set; }

        /// <summary>
        /// Creation time of this stack.
        /// </summary>
        [JsonProperty("creation_time")]
        public string CreationTime { get; set; }

        /// <summary>
        /// Deletion time of this stack.
        /// </summary>
        [JsonProperty("deletion_time")]
        public string DeletionTime { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// A boolean indicating whether the rollback is disabled or not.
        /// </summary>
        [JsonProperty("disable_rollback")]
        public bool DisableRollback { get; set; }

        /// <summary>
        /// The UUID of the stack.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// A list of URLs for the stack. Each URL is a JSON object with an href key indicating the URL and a rel key
        /// indicating its relationship to the stack in question. There may be multiple links returned. The self
        /// relationship identifies the URL of the stack itself.
        /// </summary>
        [JsonProperty("links")]
        public List<Link> Links { get; set; }
        
        /// <summary>
        /// Notification Topics
        /// </summary>
        [JsonProperty("notification_topics")]
        public List<StackNotificationTopic> NotificationTopics { get; set; }

        /// <summary>
        /// Outputs
        /// </summary>
        [JsonProperty("outputs")]
        public List<StackOutput> Outputs { get; set; }
        
        /// <summary>
        /// Parent of this stack
        /// </summary>
        [JsonProperty("parent")]
        public Stack Parent { get; set; }

        /// <summary>
        /// The name of the stack.
        /// </summary>
        [JsonProperty("stack_name")]
        public string StackName { get; set; }

        /// <summary>
        /// The owner of the stack.
        /// </summary>
        [JsonProperty("stack_owner")]
        public string StackOwner { get; set; }

        /// <summary>
        /// The status of the stack.
        /// </summary>
        [JsonProperty("stack_status")]
        public string StackStatus { get; set; }
        
        /// <summary>
        /// The status reason of the stack.
        /// </summary>
        [JsonProperty("stack_status_reason")]
        public string StackStatusReason { get; set; }

        /// <summary>
        /// The status reason of the stack.
        /// </summary>
        [JsonProperty("stack_user_project_id")]
        public string StackUserProjectId { get; set; }

        /// <summary>
        /// Tags for this stack.
        /// </summary>
        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        /// <summary>
        /// Template description.
        /// </summary>
        [JsonProperty("template_description")]
        public string TemplateDescription { get; set; }

        /// <summary>
        /// Timeout minutes.
        /// </summary>
        [JsonProperty("timeout_mins")]
        public int? TimeoutMins { get; set; }

        /// <summary>
        /// Timeout minutes.
        /// </summary>
        [JsonProperty("updated_time")]
        public string UpdatedTime { get; set; }
    }
}
