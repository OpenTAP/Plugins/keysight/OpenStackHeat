﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using Newtonsoft.Json;
using System;

namespace OpenTap.Plugins.OpenStackHeat.Objects
{
    /// <summary>
    /// Authentication Response
    /// </summary>
    [JsonObject()]
    public class AuthenticationResponse
    {
        /// <summary>
        /// (Since v3.2) The ID of the region that contains the service endpoint. 
        /// </summary>
        [JsonProperty("region_id")]
        public string RegionId { get; set; }

        /// <summary>
        /// The authentication method.For password authentication, specify password.
        /// </summary>
        [JsonProperty("methods")]
        public Array Methods { get; set; }

        /// <summary>
        /// A list of role objects
        /// </summary>
        [JsonProperty("roles")]
        public Array Roles { get; set; }

        /// <summary>
        /// The endpoint URL.
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }

        /// <summary>
        /// (Deprecated in v3.2) The geographic location of the service endpoint.
        /// </summary>
        [JsonProperty("region")]
        public string Region { get; set; }

        /// <summary>
        /// A token object.
        /// </summary>
        [JsonProperty("token")]
        public object Token { get; set; }

        /// <summary>
        /// The date and time when the token expires.
        /// The date and time stamp format is ISO 8601:
        /// 
        /// CCYY-MM-DDThh:mm:ss.sssZ
        /// For example, 2015-08-27T09:49:58.000000Z.
        /// 
        /// A null value indicates that the token never expires.
        /// </summary>
        [JsonProperty("expires_at")]
        public string ExpiresAt { get; set; }

        /// <summary>
        /// A system object containing information about which parts of the system the token is scoped to. If the token
        /// is scoped to the entire deployment system, the system object will consist of { "all": true}. This is only
        /// included in tokens that are scoped to the system.
        /// (Optional)
        /// </summary>
        [JsonProperty("system")]
        public object System { get; set; }

        /// <summary>
        /// A domain object including the id and name representing the domain the token is scoped to.This is only
        /// included in tokens that are scoped to a domain.
        /// (Optional)
        /// </summary>
        [JsonProperty("domain")]
        public object Domain { get; set; }

        /// <summary>
        /// A project object including the id, name and domain object representing the project the token is scoped to.
        /// This is only included in tokens that are scoped to a project. 
        /// (Optional)
        /// </summary>
        [JsonProperty("project")]
        public object Project { get; set; }

        /// <summary>
        /// The date and time when the token was issued.
        /// The date and time stamp format is ISO 8601:
        /// CCYY-MM-DDThh:mm:ss.sssZ
        /// For example, 2015-08-27T09:49:58.000000Z.
        /// </summary>
        [JsonProperty("issued_at")]
        public string IssuedAt { get; set; }

        /// <summary>
        /// A catalog object.
        /// </summary>
        [JsonProperty("catalog")]
        public Array Catalog { get; set; }

        /// <summary>
        /// A user object.
        /// </summary>
        [JsonProperty("user")]
        public object User { get; set; }

        /// <summary>
        /// A list of one or two audit IDs.An audit ID is a unique, randomly generated, URL-safe string that you can
        /// use to track a token.The first audit ID is the current audit ID for the token. The second audit ID is
        /// present for only re-scoped tokens and is the audit ID from the token before it was re-scoped.A re- scoped
        /// token is one that was exchanged for another token of the same or different scope. You can use these audit
        /// IDs to track the use of a token or chain of tokens across multiple requests and endpoints without exposing
        /// the token ID to non-privileged users. 
        /// </summary>
        [JsonProperty("audit_ids")]
        public Array AuditIds { get; set; }

        /// <summary>
        /// The interface type, which describes the visibility of the endpoint.Value is: - public. Visible by end users
        /// on a publicly available network interface. - internal. Visible by end users on an unmetered internal
        /// network interface. - admin.Visible by administrative users on a secure network interface.
        /// </summary>
        [JsonProperty("interface")]
        public string Interface { get; set; }

        /// <summary>
        /// A list of endpoint objects.
        /// </summary>
        [JsonProperty("endpoints")]
        public Array EndPoints { get; set; }

        /// <summary>
        /// The endpoint type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// The ID of the user. Required if you do not specify the user name.
        /// (Optional)
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// The user name. Required if you do not specify the ID of the user.If you specify the user name, you must
        /// also specify the domain, by ID or name.
        /// (Optional)
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
