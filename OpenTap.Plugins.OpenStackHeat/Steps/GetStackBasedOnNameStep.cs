﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;
using OpenTap.Plugins.OpenStackHeat.Objects;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display(Name: "Get Stack Based On Name", Description: "Get stack object based on a stack name.", Group: "Heat")]
    public class GetStackBasedOnNameStep : TestStep
    {
        #region Settings
        /// <summary>
        /// Stack name
        /// </summary>
        [Display(Name: "Stack Name", Description: "Name of the stack that should be retrieved", Group: "Context")]
        public string StackNameInput { get; set; }

        /// <summary>
        /// Orchestration Service
        /// </summary>
        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        [Display(Name: "Stack", Description: "The found stack", Group: "Output")]
        [Output]
        public Stack Stack { get; set; }
        #endregion

        public GetStackBasedOnNameStep()
        {
            Name = "Get Stack Based On Name";

            Rules.Add(() => StackNameInput != null, "A Stack Name must be provided.", "StackNameInput");
            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
        }

        public override void Run()
        {
            Stack = OrchestrationService.GetStack(StackNameInput);
            if (Stack == null)
            {
                Log.Warning($"No stack found with name {StackNameInput}");
                UpgradeVerdict(Verdict.Fail);
            }
            else
            {
                UpgradeVerdict(Verdict.Pass);
            }
        }
    }
}
