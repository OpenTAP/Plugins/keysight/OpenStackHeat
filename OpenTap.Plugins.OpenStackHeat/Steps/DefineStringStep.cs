﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display(Name: "Define String", Description: "Defines a string.", Group: "Heat")]
    public sealed class DefineStringStep : TestStep
    {
        #region Settings

        [Display(Name: "Value")]
        [Output]
        public string Value { get; set; }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="DefineStringStep">DefineStringStep</see> class.
        /// </summary>
        public DefineStringStep()
        {
            Name = "Define String";

            Rules.Add(() => Value != null, "A value is required.", "Value");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            // doing nothing, not even updating the verdict
        }
    }
}
