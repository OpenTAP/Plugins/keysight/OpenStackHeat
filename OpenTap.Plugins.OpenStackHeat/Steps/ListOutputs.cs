﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;
using OpenTap.Plugins.OpenStackHeat.Objects;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display("List Outputs", Description: "Lists outputs for a stack.", Group: "Heat")]
    public sealed class ListOutputs : TestStep
    {
        #region Settings

        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        [Display(Name: "Stack", Description: "The stack to get outputs for", Group: "Context")]
        public Input<Stack> Stack { get; set; }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="ListOutputs">ListOutputs</see> class.
        /// </summary>
        public ListOutputs()
        {
            Name = "List Outputs";
            Stack = new Input<Stack>();

            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
            Rules.Add(() => Stack.Step != null /*|| Stack.Value != null*/, "A stack is required.", "Stack");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            OrchestrationService.ListOutputs(Stack.Value.StackName, Stack.Value.Id);
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
