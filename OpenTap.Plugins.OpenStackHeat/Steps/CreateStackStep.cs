﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;
using OpenTap.Plugins.OpenStackHeat.Objects;
using System;
using System.IO;
using System.Linq;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display(Name: "Create Stack", Description: "Creates a stack.", Group: "Heat")]
    public sealed class CreateStackStep : TestStep
    {
        private string hotContentAsBase64 = null;

        #region Settings

        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        [Display(Name: "Name", Description: "The name of the stack to be created", Group: "Settings")]
        public string StackName { get; set; }

        [FilePath()]
        [Display(Name: "HOT File", Description: "Heat Orchestration Template File", Group: "Settings")]
        public string Hot
        {
            get
            {
                return hotContentAsBase64;
            }
            set
            {
                bool fallback = true;
                FileInfo fi = null;
                try
                {
                    fi = new FileInfo(value);
                    if (fi.Exists)
                    {
                        string hotFileContent = File.ReadAllText(value);
                        hotContentAsBase64 = Base64Encoding.EncodeBase64(hotFileContent);
                        fallback = false;
                    }
                }
                catch
                {
                }

                if(fallback)
                {
                    //if (Base64Encoding.IsBase64(value))
                    //{
                        hotContentAsBase64 = value;
                    //}
                    //else
                    //{
                    //    hotContentAsBase64 = null;
                    //}
                }
            }
        }

        [Display(Name: "Stack", Description: "The created stack", Group: "Output")]
        [Output]
        public Stack Stack { get; set; }

        double timeout = 600;
        [Unit("s")]
        [Display(Name: "Timeout", Description: "The timeout to wait for the stack to be created.", Group: "Settings")]
        public double Timeout
        {
            get { return timeout; }
            set
            {
                if (value >= 0)
                    timeout = value;
                else throw new Exception("Timeout must be positive");
            }
        }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="CreateStackStep">CreateStackStep</see> class.
        /// </summary>
        public CreateStackStep()
        {
            Name = "Create '{Name}'";

            Rules.Add(() => !string.IsNullOrEmpty(StackName), "A stack name is required.", "StackName");
            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
            Rules.Add(() => !string.IsNullOrEmpty(Hot) && Base64Encoding.IsBase64(Hot), "The HOT file does not exist.", "Hot");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            Stack = OrchestrationService.CreateStack(StackName, hotContentAsBase64, Timeout);
            if (Stack.StackStatus == "CREATE_COMPLETE")
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }
}
