﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;
using OpenTap.Plugins.OpenStackHeat.Objects;
using System;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display(Name: "Delete Stack", Description: "Deletes a stack. If a stack has snapshots, those snapshots are deleted as well.", Group: "Heat")]
    public sealed class DeleteStackStep : TestStep
    {
        #region Settings

        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        [Display(Name: "Stack", Description: "The stack to be deleted", Group: "Context")]
        public Input<Stack> Stack { get; set; }

        double timeout = 600;
        [Unit("s")]
        [Display(Name: "Timeout", Description: "The timeout to wait for the stack to be created.", Group: "Settings")]
        public double Timeout
        {
            get { return timeout; }
            set
            {
                if (value >= 0)
                    timeout = value;
                else throw new Exception("Timeout must be positive");
            }
        }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="DeleteStackStep">DeleteStackStep</see> class.
        /// </summary>
        public DeleteStackStep()
        {
            Name = "Delete Stack";
            Stack = new Input<Stack>();

            Rules.Add(() => Stack.Step != null, "A stack is required.", "Stack");
            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            DoDelete();
        }

        private void DoDelete()
        {
            bool deleted = OrchestrationService.DeleteStack(Stack.Value.StackName, Stack.Value.Id, Timeout);
            if (deleted)
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }
}
