﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display(Name: "List Stacks", Description: "Lists active stacks.", Group: "Heat")]
    public sealed class ListStacksStep : TestStep
    {
        #region Settings

        /// <summary>
        /// Orchestration Service
        /// </summary>
        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="ListStacksStep">ListStacksStep</see> class.
        /// </summary>
        public ListStacksStep()
        {
            Name = "List Stacks";

            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            OrchestrationService.ListStacks();
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
