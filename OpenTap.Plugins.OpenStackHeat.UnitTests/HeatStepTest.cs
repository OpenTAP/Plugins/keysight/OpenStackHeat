﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using Newtonsoft.Json;
using NUnit.Framework;
using OpenTap.Plugins.OpenStackHeat.Objects;

namespace OpenTap.Plugins.OpenStackHeat.UnitTests
{
    [TestFixture]
    public class HeatStepTest
    {
        [Test]
        public void TestDeserializeStack()
        {
            string serializedStack = "{\"stack\": {\"id\": \"2db58428-3889-437d-b129-ca937e815f26\", \"links\": [{\"href\": \"http://10.114.182.52:8004/v1/1bec4f55991541658c85451694d3fe54/stacks/ABCD/2db58428-3889-437d-b129-ca937e815f26\", \"rel\": \"self\"}]}}";
            //string serializedStack = "{\"stack\": {\"id\": \"2db58428-3889-437d-b129-ca937e815f26\"}}";
            //string serializedStack = "{\"id\": \"2db58428-3889-437d-b129-ca937e815f26\"}";
            //string serializedStack = "{\"id\": \"2db58428-3889-437d-b129-ca937e815f26\", \"links\": [{\"href\": \"http://10.114.182.52:8004/v1/1bec4f55991541658c85451694d3fe54/stacks/ABCD/2db58428-3889-437d-b129-ca937e815f26\", \"rel\": \"self\"}]}";

            StackContainer stackContainer = JsonConvert.DeserializeObject<StackContainer>(serializedStack);
            Assert.IsNotNull(stackContainer.Stack, "The stack has not been serialized");
            Assert.AreEqual("2db58428-3889-437d-b129-ca937e815f26", stackContainer.Stack.Id);
        }

        [Test]
        public void TestDeserializeShowStackInfo()
        {
            string serializedStack = @"{ 'stack':
                                         {
                                             'capabilities': [],
                                             'creation_time': '2014-06-03T20:59:46Z',
                                             'deletion_time': null,
                                             'description': 'sample stack',
                                             'disable_rollback': true,
                                             'id': '3095aefc-09fb-4bc7-b1f0-f21a304e864c',
                                             'links': [
                                                 {
                                                     'href': 'http://192.168.123.200:8004/v1/eb1c63a4f77141548385f113a28f0f52/stacks/simple_stack/3095aefc-09fb-4bc7-b1f0-f21a304e864c',
                                                     'rel': 'self'
                                                 }
                                             ],
                                             'notification_topics': [],
                                             'outputs': [],
                                             'parameters': {
                                                 'OS::project_id': '3ab5b02f-a01f-4f95-afa1-e254afc4a435',
                                                 'OS::stack_id': '3095aefc-09fb-4bc7-b1f0-f21a304e864c',
                                                 'OS::stack_name': 'simple_stack'
                                             },
                                             'parent': null,
                                             'stack_name': 'simple_stack',
                                             'stack_owner': 'simple_username',
                                             'stack_status': 'CREATE_COMPLETE',
                                             'stack_status_reason': 'Stack CREATE completed successfully',
                                             'stack_user_project_id': '65728b74-cfe7-4f17-9c15-11d4f686e591',
                                             'tags': null,
                                             'template_description': 'sample stack',
                                             'timeout_mins': null,
                                             'updated_time': null
                                         }
                                       }";

            StackContainer stackContainer = JsonConvert.DeserializeObject<StackContainer>(serializedStack);
            Assert.IsNotNull(stackContainer.Stack, "The stack has not been serialized");
            Assert.IsEmpty(stackContainer.Stack.Capabilities);
            Assert.AreEqual("2014-06-03T20:59:46Z", stackContainer.Stack.CreationTime);
            Assert.AreEqual(null, stackContainer.Stack.DeletionTime);
            Assert.AreEqual("sample stack", stackContainer.Stack.Description);
            Assert.AreEqual(true, stackContainer.Stack.DisableRollback);
            Assert.AreEqual("3095aefc-09fb-4bc7-b1f0-f21a304e864c", stackContainer.Stack.Id);
            Assert.IsNotNull(stackContainer.Stack.Links);
            Assert.AreEqual(1, stackContainer.Stack.Links.Count);
            Assert.AreEqual("self", stackContainer.Stack.Links[0].Relation);
            Assert.AreEqual("http://192.168.123.200:8004/v1/eb1c63a4f77141548385f113a28f0f52/stacks/simple_stack/3095aefc-09fb-4bc7-b1f0-f21a304e864c", stackContainer.Stack.Links[0].HRef);
            Assert.IsEmpty(stackContainer.Stack.NotificationTopics);
            Assert.IsEmpty(stackContainer.Stack.Outputs);
            // -> parameters not yet implemented
            Assert.IsNull(stackContainer.Stack.Parent);
            Assert.AreEqual("simple_stack", stackContainer.Stack.StackName);
            Assert.AreEqual("simple_username", stackContainer.Stack.StackOwner);
            Assert.AreEqual("CREATE_COMPLETE", stackContainer.Stack.StackStatus);
            Assert.AreEqual("Stack CREATE completed successfully", stackContainer.Stack.StackStatusReason);
            Assert.AreEqual("65728b74-cfe7-4f17-9c15-11d4f686e591", stackContainer.Stack.StackUserProjectId);
            Assert.IsNull(stackContainer.Stack.Tags);
            Assert.AreEqual("sample stack", stackContainer.Stack.TemplateDescription);
            Assert.IsNull(stackContainer.Stack.TimeoutMins);
            Assert.IsNull(stackContainer.Stack.UpdatedTime);
        }

        [Test]
        public void TestDeserializeShowStackInfo2()
        {
            string serializedStack =
                @"{
                    'stack':
                    {
                      'stack_name': 'DEMO',
                      'id': '43714ecf-a9dc-4b08-ac74-049578f0f00d',
                      'links':
                      [
                        {
                          'href': 'http://10.114.182.105:8004/v1/008dbb1590164528b197bac780288ce3/stacks/DEMO/43714ecf-a9dc-4b08-ac74-049578f0f00d',
                          'rel': 'self'
                        }
                      ],
                      'creation_time': '2019-03-28T09:42:20Z',
                      'updated_time': null,
                      'deletion_time': null,
                      'notification_topics': [],
                      'parameters':
                      {
                        'OS::stack_id': '43714ecf-a9dc-4b08-ac74-049578f0f00d',
                        'OS::project_id': '008dbb1590164528b197bac780288ce3',
                        'OS::stack_name': 'DEMO',
                        'flavor': 'm1.tiny'
                      },
                      'description': 'Example to test heat commands',
                      'template_description': 'Example to test heat commands',
                      'capabilities': [],
                      'disable_rollback': true,
                      'timeout_mins': 60,
                      'stack_owner': null,
                      'parent': null,
                      'stack_user_project_id': '008dbb1590164528b197bac780288ce3',
                      'tags': [ 'My First Stack' ],
                      'stack_status': 'CREATE_FAILED',
                      'stack_status_reason': 'Resource CREATE failed: ValueError: resources.hello_world: nics are required after microversion 2.36',
                      'outputs': []
                    }
                  }";

            StackContainer stackContainer = JsonConvert.DeserializeObject<StackContainer>(serializedStack);
            Assert.IsNotNull(stackContainer.Stack, "The stack has not been serialized");
            Assert.IsEmpty(stackContainer.Stack.Capabilities);
            Assert.AreEqual("2019-03-28T09:42:20Z", stackContainer.Stack.CreationTime);
            Assert.AreEqual(null, stackContainer.Stack.DeletionTime);
            Assert.AreEqual("Example to test heat commands", stackContainer.Stack.Description);
            Assert.AreEqual(true, stackContainer.Stack.DisableRollback);
            Assert.AreEqual("43714ecf-a9dc-4b08-ac74-049578f0f00d", stackContainer.Stack.Id);
            Assert.IsNotNull(stackContainer.Stack.Links);
            Assert.AreEqual(1, stackContainer.Stack.Links.Count);
            Assert.AreEqual("self", stackContainer.Stack.Links[0].Relation);
            Assert.AreEqual("http://10.114.182.105:8004/v1/008dbb1590164528b197bac780288ce3/stacks/DEMO/43714ecf-a9dc-4b08-ac74-049578f0f00d", stackContainer.Stack.Links[0].HRef);
            Assert.IsEmpty(stackContainer.Stack.NotificationTopics);
            Assert.IsEmpty(stackContainer.Stack.Outputs);
            // -> parameters not yet implemented
            Assert.IsNull(stackContainer.Stack.Parent);
            Assert.AreEqual("DEMO", stackContainer.Stack.StackName);
            Assert.AreEqual(null, stackContainer.Stack.StackOwner);
            Assert.AreEqual("CREATE_FAILED", stackContainer.Stack.StackStatus);
            Assert.AreEqual("Resource CREATE failed: ValueError: resources.hello_world: nics are required after microversion 2.36", stackContainer.Stack.StackStatusReason);
            Assert.AreEqual("008dbb1590164528b197bac780288ce3", stackContainer.Stack.StackUserProjectId);
            Assert.IsNotNull(stackContainer.Stack.Tags);
            Assert.AreEqual(1, stackContainer.Stack.Tags.Count);
            Assert.AreEqual("My First Stack", stackContainer.Stack.Tags[0]);
            Assert.AreEqual("Example to test heat commands", stackContainer.Stack.TemplateDescription);
            Assert.AreEqual(60, stackContainer.Stack.TimeoutMins);
            Assert.IsNull(stackContainer.Stack.UpdatedTime);
        }

        [Test]
        public void DeserializeOutput()
        {
            string serializedOutput = @"{ 'output_key': 'output_name', 'output_value': 'output_value', 'description': 'output description', 'output_error': 'error message' }";
            StackOutput stackOutput = JsonConvert.DeserializeObject<StackOutput>(serializedOutput);
            Assert.AreEqual("output_name", stackOutput.Key);
            Assert.AreEqual("output_value", stackOutput.Value);
            Assert.AreEqual("output description", stackOutput.Description);
            Assert.AreEqual("error message", stackOutput.Error);

            //string serializedOutput2 = @"'output': { 'output_key': 'output_name', 'output_value': 'output_value', 'description': 'output description', 'output_error': 'error message' } }";
            //StackOutput stackOutput2 = JsonConvert.DeserializeObject<StackOutput>(serializedOutput2);
            //Assert.AreEqual("output_name", stackOutput2.Key);
            //Assert.AreEqual("output_value", stackOutput2.Value);
            //Assert.AreEqual("output description", stackOutput2.Description);
            //Assert.AreEqual("error message", stackOutput2.Error);
        }

        [Test]
        public void DeserializeStackEventArray()
        {
            Assert.IsTrue(true);
            //            {
            //                "stacks": [
            //                  {
            //      "id": "e97f8221-f91e-4cb0-bd56-65a7f3852c59",
            //                    "links": [
            //                      {
            //          "href": "http://10.114.182.105:8004/v1/008dbb1590164528b197bac780288ce3/stacks/BBBB/e97f8221-f91e-4cb0-bd56-65a7f3852c59",
            //                        "rel": "self"
            //        }
            //      ],
            //      "stack_name": "BBBB",
            //      "description": "",
            //      "stack_status": "DELETE_IN_PROGRESS",
            //      "stack_status_reason": "Stack DELETE started",
            //      "creation_time": "2019-02-28T21:03:55Z",
            //      "updated_time": "2019-02-28T21:03:56Z",
            //      "deletion_time": null,
            //      "stack_owner": null,
            //      "parent": null,
            //      "stack_user_project_id": null,
            //      "tags": [ "My First Stack" ]
            //}
            //  ]
            //}
        }

        [Test]
        public void DeserializeEventArray()
        {
            string serializedEventArray =
            @"{
                'events': [
                    {
                        'event_time': '2014-07-23T08:14:47Z',
                        'id': '474bfdf0-a450-46ec-a78a-0c7faa404073',
                        'links': [
                            {
                                'href': 'http://192.168.123.200:8004/v1/dc4b074874244f7693dd65583733a758/stacks/aws_port/db467ed1-50b5-4a3e-aeb1-396ff1d151c5/resources/port/events/474bfdf0-a450-46ec-a78a-0c7faa404073',
                                'rel': 'self'
                            },
                            {
                                'href': 'http://192.168.123.200:8004/v1/dc4b074874244f7693dd65583733a758/stacks/aws_port/db467ed1-50b5-4a3e-aeb1-396ff1d151c5/resources/port',
                                'rel': 'resource'
                            },
                            {
                                'href': 'http://192.168.123.200:8004/v1/dc4b074874244f7693dd65583733a758/stacks/aws_port/db467ed1-50b5-4a3e-aeb1-396ff1d151c5',
                                'rel': 'stack'
                            }
                        ],
                        'logical_resource_id': 'port',
                        'physical_resource_id': null,
                        'resource_name': 'port',
                        'resource_status': 'CREATE_FAILED',
                        'resource_status_reason': 'NotFound: Subnet f8a699d0-3537-429e-87a5-6b5a8d0c2bf0 could not be found'
                    },
                    {
                        'event_time': '2014-07-23T08:14:47Z',
                        'id': '66fa95b6-e6f8-4f05-b1af-e828f5aba04c',
                        'links': [
                            {
                                'href': 'http://192.168.123.200:8004/v1/dc4b074874244f7693dd65583733a758/stacks/aws_port/db467ed1-50b5-4a3e-aeb1-396ff1d151c5/resources/port/events/66fa95b6-e6f8-4f05-b1af-e828f5aba04c',
                                'rel': 'self'
                            },
                            {
                                'href': 'http://192.168.123.200:8004/v1/dc4b074874244f7693dd65583733a758/stacks/aws_port/db467ed1-50b5-4a3e-aeb1-396ff1d151c5/resources/port',
                                'rel': 'resource'
                            },
                            {
                                'href': 'http://192.168.123.200:8004/v1/dc4b074874244f7693dd65583733a758/stacks/aws_port/db467ed1-50b5-4a3e-aeb1-396ff1d151c5',
                                'rel': 'stack'
                            }
                        ],
                        'logical_resource_id': 'port',
                        'physical_resource_id': null,
                        'resource_name': 'port',
                        'resource_status': 'CREATE_IN_PROGRESS',
                        'resource_status_reason': 'state changed'
                    }
                ]
            }";

            ListStackEventsResponse lser = JsonConvert.DeserializeObject<ListStackEventsResponse>(serializedEventArray);
            Assert.AreEqual(2, lser.Events.Count);
            Assert.AreEqual("CREATE_FAILED", lser.Events[0].ResourceStatus);
            Assert.AreEqual("CREATE_IN_PROGRESS", lser.Events[1].ResourceStatus);
        }

        [Test]
        public void TestDeserializeVersions()
        {
            string serializedVersions = @"{
                                            'versions':
                                            [
                                                {
                                                    'status': 'CURRENT',
                                                    'id': 'v1.0',
                                                    'links':
                                                    [
                                                        {
                                                            'href': 'http://23.253.228.211:8000/v1/',
                                                            'rel': 'self'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }";

            GetVersionsResponse gvr = JsonConvert.DeserializeObject<GetVersionsResponse>(serializedVersions);
            Assert.AreEqual(1, gvr.Versions.Count);
            Assert.AreEqual("CURRENT", gvr.Versions[0].Status);
            Assert.AreEqual("v1.0", gvr.Versions[0].Id);
            Assert.IsNotNull(gvr.Versions[0].Links);
            Assert.AreEqual(1, gvr.Versions[0].Links.Count);
            Assert.AreEqual("http://23.253.228.211:8000/v1/", gvr.Versions[0].Links[0].HRef);
            Assert.AreEqual("self", gvr.Versions[0].Links[0].Relation);
        }
    }
}